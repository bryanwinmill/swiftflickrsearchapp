//
//  FlickrPhoto.swift
//  SwiftFlickrSearch
//
//  Created by Bryan Winmill on 10/19/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit

struct FlickrPhoto {
    
    let photoID: String
    let farm: Int
    let secret: String
    let server: String
    let title: String
    
    var photoUrl: URL {
        return URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(photoID))\(secret)_m.jpg")!
    }
    
}
