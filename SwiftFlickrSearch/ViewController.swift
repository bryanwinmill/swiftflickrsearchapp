//
//  ViewController.swift
//  SwiftFlickrSearch
//
//  Created by Bryan Winmill on 10/18/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let apiKey = "1dd17dde0fed7286935d83875fcc17dd"
    
    var flickrPictures: [String] = []
    
    @IBOutlet var searchBar: UISearchBar!
    
    @IBOutlet var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        searchBar.delegate = self
        
        //self.collectionView.register(UINib(nibName: "MyCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        DispatchQueue.main.async {
            self.flickrPictures.removeAll()
            self.collectionView.reloadData()
        }
        
        if (searchBar.text?.isEmpty)! {
            print("searchBar has no text!")
        } else {
            
            let url = flickrSearchURLForSearchTerm(searchBar.text!)
            print(url as Any)
            
            let task = URLSession.shared.dataTask(with: url!, completionHandler: { data, response, error in
                
                guard error == nil else {
                    print(error!)
                    return
                }
                guard let data = data else {
                    print("Data is empty")
                    return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
                    
                    guard let results = json else { return }
                    
                    if let status = results["code"] as? Int {
                        if status == 0 {
                            return
                        }
                    }
                    
                    guard let photo = json!["photos"] as? NSDictionary else { return }
                    guard let photoArray = photo["photo"] as? [NSDictionary] else { return }
                    
                    let _: [FlickrPhoto] = photoArray.map { photoDictionary in
                        
                        let photoID = photoDictionary["id"] as? String ?? ""
                        let farm = photoDictionary["farm"] as? Int ?? 0
                        let secret = photoDictionary["secret"] as? String ?? ""
                        let server = photoDictionary["server"] as? String ?? ""
                        let title = photoDictionary["title"] as? String ?? ""
                        
                        self.flickrPictures.append("https://farm\(farm).staticflickr.com/\(server)/\(photoID)_\(secret).jpg")
                        
                        let flickrPhoto = FlickrPhoto (photoID: photoID, farm: farm, secret: secret, server: server, title: title)
                        
                        return flickrPhoto
                    }
                    
                } catch _ as NSError {
                    return
                }
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
            })
            
            task.resume()
            
        }
        
        self.searchBar.endEditing(true)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (flickrPictures as AnyObject).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let imageView:UIImageView=UIImageView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width/2, height: self.view.frame.size.width/2))
        
        let imageURL = URL(string: flickrPictures[indexPath.row])
        
        DispatchQueue.global().async {
            let data = try? Data(contentsOf: imageURL!)
            DispatchQueue.main.async {
                let image:UIImage = UIImage(data: data!)!
                imageView.image = image
                cell.contentView.addSubview(imageView)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.size.width/2 - 20, height: self.view.frame.size.width/2 - 20)
    }
    
    fileprivate func flickrSearchURLForSearchTerm(_ searchTerm:String) -> URL? {
        
        guard let escapedTerm = searchTerm.addingPercentEncoding(withAllowedCharacters: CharacterSet.alphanumerics) else {
            return nil
        }
        
        let URLString = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&text=\(escapedTerm)&per_page=20&format=json&nojsoncallback=1"
        
        guard let url = URL(string:URLString) else {
            return nil
        }
        
        return url
    }
    

}

